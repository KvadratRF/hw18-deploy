<?php

namespace Tests;

use App\Modules\Queries\Application\CreateQueryAction;
use App\Modules\Queries\Domain\QueryStatusEnum;

class GetQueryTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_query_404_response()
    {
        $response = $this->get('/api/v1/queries/1');

        $response->assertResponseStatus(404);
    }

    public function test_create_query_a_success_response()
    {
        CreateQueryAction::run('test');

        $response = $this->get('/api/v1/queries/1');

        $response->assertResponseOk();

        $response->seeJson([
            'id' => 1,
            'name' => 'test',
            'status' => QueryStatusEnum::ready->value,
        ]);
    }
}
