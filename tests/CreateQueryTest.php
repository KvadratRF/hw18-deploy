<?php

namespace Tests;

class CreateQueryTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_create_query_a_successful_response()
    {
        $response = $this->post('/api/v1/queries/create', [
            'name' => 'test',
        ]);

        $response->assertResponseOk();

        $response->seeJsonEquals([
            'id' => 1,
        ]);

        $this->seeInDatabase('queries', [
            'name' => 'test',
        ]);
    }

    public function test_create_query_422_response()
    {
        $response = $this->post('/api/v1/queries/create');

        $response->assertResponseStatus(422);
    }
}
